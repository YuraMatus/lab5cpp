package com.teamname.laba5.storage;

import com.teamname.laba5.item.Engine;
import com.teamname.ui.MainForm;
import com.teamname.ui.TableModelWrapper;
import lombok.extern.log4j.Log4j2;

import java.util.concurrent.BlockingQueue;

@Log4j2
public class EngineStorage implements Runnable {
	private final BlockingQueue<Engine> supplierToStorageQueue;
	private final BlockingQueue<Engine> engineStorageToCombinerQueue;
	private TableModelWrapper defaultStorageTableModelWrapper;

	public EngineStorage(final BlockingQueue<Engine> supplierToStorageQueue, final BlockingQueue<Engine> engineStorageToCombinerQueue,
						 TableModelWrapper model) {
		this.supplierToStorageQueue = supplierToStorageQueue;
		this.engineStorageToCombinerQueue = engineStorageToCombinerQueue;
		this.defaultStorageTableModelWrapper = model;
	}

	@Override
	public void run() {
		try {
			while (true) {
				if (MainForm.isTerminated.get()) {
					log.info("EngineStorage closed");
					return;
				}
				while (MainForm.isPaused)
				{
					Thread.sleep(1000);
				}

				final Engine receivedEngine = supplierToStorageQueue.take();

				if(receivedEngine != null) {
					defaultStorageTableModelWrapper.increment(0,1);
					Thread.sleep(1200);

					log.info("received engine with id {}", receivedEngine.getEngineId());
					engineStorageToCombinerQueue.put(receivedEngine);
					defaultStorageTableModelWrapper.decrement(0,1);
					Thread.sleep(1200);

					log.info("sent engine with id {} to combiner", receivedEngine.getEngineId());
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
