package com.teamname.laba5.storage;

import com.teamname.laba5.item.Body;
import com.teamname.ui.MainForm;
import com.teamname.ui.TableModelWrapper;
import lombok.extern.log4j.Log4j2;

import javax.swing.table.DefaultTableModel;
import java.util.concurrent.BlockingQueue;

@Log4j2
public class BodyStorage implements Runnable {
	private final BlockingQueue<Body> supplierToStorageQueue;
	private final BlockingQueue<Body> bodyStorageToCombinerQueue;
	private TableModelWrapper storageTableWrapper;

	public BodyStorage(final BlockingQueue<Body> supplierToStorageQueue, final BlockingQueue<Body> bodyStorageToCombinerQueue,
					   TableModelWrapper wrapper) {
		this.supplierToStorageQueue = supplierToStorageQueue;
		this.bodyStorageToCombinerQueue = bodyStorageToCombinerQueue;
		this.storageTableWrapper = wrapper;
	}

	@Override
	public void run() {
		try {
			while (true) {
				if (MainForm.isTerminated.get()) {
					log.info("BodyStorage closed");
					return;
				}
				while (MainForm.isPaused)
				{
					Thread.sleep(1000);
				}

				final Body receivedBody = supplierToStorageQueue.take();

				if(receivedBody != null) {
					storageTableWrapper.increment(0,0);
					Thread.sleep(1000);

					log.info("received body with id {}", receivedBody.getBodyId());
					bodyStorageToCombinerQueue.put(receivedBody);
					storageTableWrapper.decrement(0,0);
					Thread.sleep(1000);

					log.info("sent body to with id {} to combiner", receivedBody.getBodyId());
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
