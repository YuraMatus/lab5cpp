package com.teamname.laba5.storage;

import com.teamname.laba5.item.Accessors;
import com.teamname.ui.MainForm;
import com.teamname.ui.TableModelWrapper;
import lombok.extern.log4j.Log4j2;

import javax.swing.table.DefaultTableModel;
import java.util.concurrent.BlockingQueue;

@Log4j2
public class AccessorsStorage implements Runnable {
	private final BlockingQueue<Accessors> supplierToStorageQueue;
	private final BlockingQueue<Accessors> accessorsStorageToCombinerQueue;
	private TableModelWrapper storageTableWrapper;

	public AccessorsStorage(final BlockingQueue<Accessors> supplierToStorageQueue, final BlockingQueue<Accessors> accessorsStorageToCombinerQueue,
							TableModelWrapper wrapper) {
		this.supplierToStorageQueue = supplierToStorageQueue;
		this.accessorsStorageToCombinerQueue = accessorsStorageToCombinerQueue;
		this.storageTableWrapper = wrapper;
	}

	@Override
	public void run() {
		try {
			while (true) {
				if (MainForm.isTerminated.get()) {
					log.info("AccessorsStorage closed");
					return;
				}
				while (MainForm.isPaused)
				{
					Thread.sleep(1000);
				}

				final Accessors receivedAccessors = supplierToStorageQueue.take();

				if(receivedAccessors != null) {
					storageTableWrapper.increment(0,2);
					Thread.sleep(1300);

					log.info("received accessors with id {}", receivedAccessors.getAccessorId());

					accessorsStorageToCombinerQueue.put(receivedAccessors);
					storageTableWrapper.decrement(0,2);
					Thread.sleep(1300);

					log.info("sent accessors with id {} to combiner", receivedAccessors.getAccessorId());
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}