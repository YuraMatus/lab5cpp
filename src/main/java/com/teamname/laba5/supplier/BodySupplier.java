package com.teamname.laba5.supplier;

import com.teamname.laba5.item.Body;
import com.teamname.ui.MainForm;
import com.teamname.ui.TableModelWrapper;
import lombok.extern.log4j.Log4j2;

import java.util.concurrent.BlockingQueue;

@Log4j2
public class BodySupplier implements Runnable {
	private final BlockingQueue<Body> queue;
	private TableModelWrapper suppliedTableModelWrapper;

	public BodySupplier(final BlockingQueue<Body> queue, TableModelWrapper suppliedTableModelWrapper) {
		this.queue = queue;
		this.suppliedTableModelWrapper = suppliedTableModelWrapper;
	}

	@Override
	public void run() {
		try {
			int i = 0;
			while (true) {
				if (MainForm.isTerminated.get()) {
					log.info("Body Supplier closed");
					return;
				}
				while (MainForm.isPaused)
				{
					Thread.sleep(1000);
				}

				Thread.sleep(MainForm.speedOfBodiesCreation.get());
				log.info("trying to supply body");

				queue.put(new Body(++i));
				suppliedTableModelWrapper.increment(0,0);
				log.info("body supplied with id: {}", i);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
