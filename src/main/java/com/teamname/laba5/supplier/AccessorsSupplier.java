package com.teamname.laba5.supplier;

import com.teamname.laba5.item.Accessors;
import com.teamname.ui.MainForm;
import com.teamname.ui.TableModelWrapper;
import lombok.extern.log4j.Log4j2;

import java.util.concurrent.BlockingQueue;

@Log4j2
public class AccessorsSupplier implements Runnable {
	private final BlockingQueue<Accessors> queue;
	private TableModelWrapper suppliedTableModelWrapper;

	public AccessorsSupplier(final BlockingQueue<Accessors> queue, TableModelWrapper suppliedTableModelWrapper) {
		this.queue = queue;
		this.suppliedTableModelWrapper = suppliedTableModelWrapper;
	}

	public void run() {
		try {
			int i = 0;
			while (true) {
				if (MainForm.isTerminated.get()) {
					log.info("Accessors Supplier closed");
					return;
				}
				while (MainForm.isPaused)
				{
					Thread.sleep(1000);
				}

				Thread.sleep(MainForm.speedOfAccessorsCreation.get());
				log.info("trying to supply accessors");
				queue.put(new Accessors(++i));
				suppliedTableModelWrapper.increment(0,2);
				log.info("accessors supplied with id:{}", i);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
