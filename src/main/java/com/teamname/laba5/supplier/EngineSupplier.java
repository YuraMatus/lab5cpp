package com.teamname.laba5.supplier;

import com.teamname.laba5.item.Engine;
import com.teamname.ui.MainForm;
import com.teamname.ui.TableModelWrapper;
import lombok.extern.log4j.Log4j2;

import java.util.concurrent.BlockingQueue;

@Log4j2
public class EngineSupplier implements Runnable {
	private final BlockingQueue<Engine> queue;
	private TableModelWrapper suppliedTableModelWrapper;

	public EngineSupplier(final BlockingQueue<Engine> queue, TableModelWrapper suppliedTableModelWrapper) {
		this.queue = queue;
		this.suppliedTableModelWrapper = suppliedTableModelWrapper;
	}

	@Override
	public void run() {
		try {
			int i = 0;
			while (true) {
				if (MainForm.isTerminated.get()) {
					log.info("Engine Supplier closed");
					return;
				}
				while (MainForm.isPaused)
				{
					Thread.sleep(1000);
				}

				Thread.sleep(MainForm.speedOfEnginesCreation.get());
				log.info("trying to supply engine");
				queue.put(new Engine(++i));
				suppliedTableModelWrapper.increment(0,1);
				log.info("engine supplied with id: {}", i);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
