package com.teamname.laba5.combiner;

import com.teamname.laba5.item.Accessors;
import com.teamname.laba5.item.Body;
import com.teamname.laba5.item.Car;
import com.teamname.laba5.item.Engine;
import com.teamname.ui.MainForm;
import com.teamname.ui.TableModelWrapper;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.util.concurrent.BlockingQueue;

@Log4j2
public class Combiner implements Runnable {
	private final BlockingQueue<Body> bodyStorageToCombinerQueue;
	private final BlockingQueue<Engine> engineStorageToCombinerQueue;
	private final BlockingQueue<Accessors> accessorsStorageToCombinerQueue;

	private final BlockingQueue<Car> combinerToDillerQueue;

	private TableModelWrapper combinerTableWrapper;

	public Combiner(BlockingQueue<Body> bodyStorageToCombinerQueue, BlockingQueue<Engine> engineStorageToCombinerQueue,
					BlockingQueue<Accessors> accessorsStorageToCombinerQueue, BlockingQueue<Car> combinerToDillerQueue,
					TableModelWrapper modelWrapper) {
		this.bodyStorageToCombinerQueue = bodyStorageToCombinerQueue;
		this.engineStorageToCombinerQueue = engineStorageToCombinerQueue;
		this.accessorsStorageToCombinerQueue = accessorsStorageToCombinerQueue;
		this.combinerToDillerQueue = combinerToDillerQueue;
		this.combinerTableWrapper = modelWrapper;
	}

	@Override
	public void run() {
		try {
			int i = 0;
			while (true) {
				if (MainForm.isTerminated.get()) {
					log.info("Combiner closed");
					return;
				}
				while (MainForm.isPaused)
				{
					Thread.sleep(1000);
				}

				Body body = bodyStorageToCombinerQueue.take();
				combinerTableWrapper.increment(0, 0);
				log.info("Combiner received body with id: {}", body.getBodyId());

				Engine engine = engineStorageToCombinerQueue.take();
				combinerTableWrapper.increment(0, 1);
				log.info("Combiner received engine with id: {}", engine.getEngineId());

				Accessors accessors = accessorsStorageToCombinerQueue.take();
				combinerTableWrapper.increment(0, 2);
				log.info("Combiner received accessors with id: {}", accessors.getAccessorId());
				Thread.sleep(1200);
				combinerTableWrapper.increment(0, 3);
				Thread.sleep(1200);

				combinerToDillerQueue.put(new Car(++i, engine, body, accessors));

				combinerTableWrapper.decrement(0, 0);
				combinerTableWrapper.decrement(0, 1);
				combinerTableWrapper.decrement(0, 2);
				combinerTableWrapper.decrement(0, 3);
				log.info("Car with id {} sent to diller by combiner", i);
				Thread.sleep(1200);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
