package com.teamname.laba5.item;

import lombok.Data;

@Data
public class Car {
	private final int carId;
	private final Engine engine;
	private final Body body;
	private final Accessors accessors;

	public Car(int carId, Engine engine, Body body, Accessors accessors) {
		this.carId = carId;
		this.engine = engine;
		this.body = body;
		this.accessors = accessors;
	}
}