package com.teamname.laba5.item;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Accessors {
	private int accessorId;
}
