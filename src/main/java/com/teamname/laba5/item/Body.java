package com.teamname.laba5.item;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Body {
	private int bodyId;
}
