package com.teamname.laba5.diller;

import com.teamname.laba5.item.Car;
import com.teamname.ui.MainForm;
import com.teamname.ui.TableModelWrapper;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import javax.swing.*;
import java.util.concurrent.BlockingQueue;

@Log4j2
public class Diller implements Runnable {
	private final BlockingQueue<Car> combinerToDillerQueue;
	private TableModelWrapper dillerTableWrapper;
	private int threadId;

	public Diller(BlockingQueue<Car> combinerToDillerQueue, TableModelWrapper dillersTableModelWrapper, int id) {
		this.combinerToDillerQueue = combinerToDillerQueue;
		this.dillerTableWrapper = dillersTableModelWrapper;
		this.threadId = id;
	}

	@Override
	public void run() {
		try{
			while (true) {
				if (MainForm.isTerminated.get()) {
					log.info("Diller closed");
					return;
				}
				while (MainForm.isPaused)
				{
					Thread.sleep(1000);
				}
				Thread.sleep(MainForm.speedOfDillerWork.get());

				Car car = combinerToDillerQueue.take();
				dillerTableWrapper.increment(threadId, 1);
				dillerTableWrapper.increment(2, 1);
				log.info("received car with id: {}", car.getCarId());
				log.info("car has accessors with id: {}", car.getAccessors().getAccessorId());
				log.info("car has engine with id: {}", car.getEngine().getEngineId());
				log.info("car has body with id: {}", car.getBody().getBodyId());
			}
		} catch (InterruptedException e) {
		e.printStackTrace();
	}
}
}
