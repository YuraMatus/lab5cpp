package com.teamname.ui;
import com.teamname.laba5.combiner.Combiner;
import com.teamname.laba5.diller.Diller;
import com.teamname.laba5.item.Accessors;
import com.teamname.laba5.item.Body;
import com.teamname.laba5.item.Car;
import com.teamname.laba5.item.Engine;
import com.teamname.laba5.storage.AccessorsStorage;
import com.teamname.laba5.storage.BodyStorage;
import com.teamname.laba5.storage.EngineStorage;
import com.teamname.laba5.supplier.AccessorsSupplier;
import com.teamname.laba5.supplier.BodySupplier;
import com.teamname.laba5.supplier.EngineSupplier;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Log4j2
public class MainForm extends JFrame{
    public static volatile boolean isPaused = false;

    private JTable storageTable;
    private JPanel mainPanel;
    private JScrollPane storageTableScrollPane;
    private JLabel storageLabel;
    private JLabel storageSizeLabel;
    private JSlider accessorSupplierSpeedSlider;
    private JSlider engineSupplierSpeedSlider;
    private JSlider bodySupplierSpeedSlider;
    private JTable supplierTable;
    private JScrollPane supplierScrollPane;
    private JLabel supplierLabel;
    private JPanel JPanelSuplyTable;
    private JPanel JPanelSuplySpeed;
    private JLabel SliderLabel;
    private JTable dillersTable;
    private JLabel dillersLabel;
    private JTable combinerTable;
    private JLabel combinerLabel;
    private JPanel combinerJPanel;
    private JPanel dillerJPanel;
    private JPanel storageJPanel;
    private JSlider dillersSpeedSlider;
    private JLabel dillerSpeedLabel;
    private JButton runButton;
    private JButton resumeButton;
    private JButton pauseButton;
    private DefaultTableModel defaultStorageTableModel;
    private DefaultTableModel defaultSuppliedTableModel;
    private DefaultTableModel defaultDillersTableModel;
    private DefaultTableModel defaultCombinerTableModel;
    private TableModelWrapper storageTableModelWrapper;
    private TableModelWrapper suppliedTableModelWrapper;
    private TableModelWrapper dillersTableModelWrapper;
    private TableModelWrapper combinerTableModelWrapper;

    public static final int numberInStorage = LazyPropertiesHolder.config.getInt("storages.capacity");
    public static final int NUMBER_OF_DETAILS_IN_STORAGE_TO_COMBINER_QUEUE = 10;
    public static final int NUMBER_OF_DETAILS_IN_COMBINER_TO_DILLER_QUEUE = 10;

    //numbers of threads of each type, just set any value
    public static AtomicInteger numberOfSuppliers = new AtomicInteger(LazyPropertiesHolder.config.getInt("suppliers.number"));
    public static AtomicInteger numberOfStorages = new AtomicInteger(LazyPropertiesHolder.config.getInt("storages.number"));
    public static AtomicInteger numberOfCombiners = new AtomicInteger(LazyPropertiesHolder.config.getInt("combiners.number"));
    public static AtomicInteger numberOfDillers = new AtomicInteger(LazyPropertiesHolder.config.getInt("dillers.number"));
    //set 'true' when you want to stop threads
    public static AtomicBoolean isTerminated = new AtomicBoolean(false);
    //speed of creation means that details will be produced every N seconds
    public static AtomicInteger speedOfEnginesCreation = new AtomicInteger(2);
    public static AtomicInteger speedOfBodiesCreation = new AtomicInteger(2);
    public static AtomicInteger speedOfAccessorsCreation = new AtomicInteger(2);
    public static AtomicInteger speedOfDillerWork = new AtomicInteger(2);

    public DefaultTableModel getDefaultStorageTableModel(){
        return defaultStorageTableModel;
    }

    public void setDefaultStorageTableModel(DefaultTableModel model){
        defaultStorageTableModel = model;
        defaultStorageTableModel.fireTableDataChanged();
    }

    public static void main(String[] args) {
        new MainForm();
    }

    private void setUp(){
        log.info("Application started");
        storageTableModelWrapper = new TableModelWrapper(defaultStorageTableModel, true);
        suppliedTableModelWrapper = new TableModelWrapper(defaultSuppliedTableModel, true);
        dillersTableModelWrapper = new TableModelWrapper(defaultDillersTableModel, false);
        combinerTableModelWrapper = new TableModelWrapper(defaultCombinerTableModel, true);

        BlockingQueue<Engine> engineSupplierToStorageQueue = new LinkedBlockingDeque<>(numberInStorage);
        BlockingQueue<Body> bodySupplierToStorageQueue = new LinkedBlockingDeque<>(numberInStorage);
        BlockingQueue<Accessors> accessorsSupplierToStorageQueue = new LinkedBlockingDeque<>(numberInStorage);

        BlockingQueue<Engine> engineStorageToCombinerQueue = new LinkedBlockingDeque<>(NUMBER_OF_DETAILS_IN_STORAGE_TO_COMBINER_QUEUE);
        BlockingQueue<Body> bodyStorageToCombinerQueue = new LinkedBlockingDeque<>(NUMBER_OF_DETAILS_IN_STORAGE_TO_COMBINER_QUEUE);
        BlockingQueue<Accessors> accessorsStorageToCombinerQueue = new LinkedBlockingDeque<>(NUMBER_OF_DETAILS_IN_STORAGE_TO_COMBINER_QUEUE);

        BlockingQueue<Car> combinerToDillerQueue = new LinkedBlockingDeque<>(NUMBER_OF_DETAILS_IN_COMBINER_TO_DILLER_QUEUE);

        ExecutorService executor = Executors.newCachedThreadPool();
        for (int i = 0; i < numberOfSuppliers.get(); i++) {
            executor.submit(new AccessorsSupplier(accessorsSupplierToStorageQueue, suppliedTableModelWrapper));
            executor.submit(new EngineSupplier(engineSupplierToStorageQueue, suppliedTableModelWrapper));
            executor.submit(new BodySupplier(bodySupplierToStorageQueue, suppliedTableModelWrapper));
        }

        for (int i = 0; i < numberOfStorages.get(); i++) {
            executor.submit(new EngineStorage(engineSupplierToStorageQueue, engineStorageToCombinerQueue, storageTableModelWrapper));
            executor.submit(new BodyStorage(bodySupplierToStorageQueue, bodyStorageToCombinerQueue, storageTableModelWrapper));
            executor.submit(new AccessorsStorage(accessorsSupplierToStorageQueue, accessorsStorageToCombinerQueue, storageTableModelWrapper));
        }

        for (int i = 0; i < numberOfCombiners.get(); i++) {
            executor.submit(new Combiner(bodyStorageToCombinerQueue, engineStorageToCombinerQueue,
                    accessorsStorageToCombinerQueue, combinerToDillerQueue, combinerTableModelWrapper));
        }

        for (int i = 0; i < numberOfDillers.get(); i++) {
            executor.submit(new Diller(combinerToDillerQueue, dillersTableModelWrapper, i));
        }


    }

    private static class LazyPropertiesHolder {
        static PropertiesConfiguration config = new PropertiesConfiguration();

        static {
            try {
                config.load("application.properties");
            } catch (ConfigurationException e) {
                e.printStackTrace();
            }
        }
    }

    public MainForm(){
        fillForm();
        runButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setUp();
                runButton.setEnabled(false);
            }
        });
        pauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                isPaused = true;
            }
        });
        resumeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                isPaused = false;
            }
        });
    }

    private void fillForm() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(0,0,720,500);

        fillStorageTable();
        fillSuppliedTable();
        fillDillersTable();
        fillCombinerTable();
        setUpSliders();

        storageTable.setModel(defaultStorageTableModel);
        supplierTable.setModel(defaultSuppliedTableModel);
        dillersTable.setModel(defaultDillersTableModel);
        combinerTable.setModel(defaultCombinerTableModel);
        setStorageSize(numberInStorage);
        getContentPane().add(mainPanel);

        runButton.setBackground(new Color(242,242,242));
        pauseButton.setBackground(new Color(242,242,242));
        ImageIcon icon = new ImageIcon("src\\main\\java\\com\\teamname\\icons\\pause.png");
        Image resizedImage = icon.getImage().getScaledInstance(16, 16,  java.awt.Image.SCALE_SMOOTH);
        pauseButton.setIcon(new ImageIcon(resizedImage));
        resumeButton.setIcon(new ImageIcon("src\\main\\java\\com\\teamname\\icons\\play-button.png"));
        resumeButton.setBackground(new Color(242,242,242));

        setVisible(true);
    }

    private String[] storageAndSupplierTableColumnNames = {"Bodies", "Engines", "Accessories"};
    private String[] combinerTableColumnNames = {"Bodies", "Engines", "Accessories", "Cars"};

    private Object[][] storageAndSupplierTableValues = {{0,0,0},
            {"Total:", 0,null}};

    private Object[][] combinerTableValues = {{0,0,0,0},
            {"Total:", 0, null, null}};

    private Object[][] dillersTableValues = new Object[numberOfDillers.get()+1][2];

    private void setStorageSize(int size) {
        storageSizeLabel.setText("Storage size: " + size);
    }

    private void fillStorageTable(){
        defaultStorageTableModel = new DefaultTableModel(storageAndSupplierTableValues, storageAndSupplierTableColumnNames);
    }

    private void fillSuppliedTable(){
        defaultSuppliedTableModel = new DefaultTableModel(storageAndSupplierTableValues, storageAndSupplierTableColumnNames);
    }

    private void fillCombinerTable(){
        defaultCombinerTableModel = new DefaultTableModel(combinerTableValues, combinerTableColumnNames);
    }

    private void fillDillersTable(){
        for(int i = 0; i < numberOfDillers.get(); i++){
            dillersTableValues[i][0] = "Diller " + (i+1);
            dillersTableValues[i][1] = 0;
        }
        dillersTableValues[numberOfDillers.get()][0] = "Total cars: ";
        dillersTableValues[numberOfDillers.get()][1] = 0;
        defaultDillersTableModel = new DefaultTableModel(dillersTableValues, new String[]{"", "Cars"});
    }

    private void setUpSliders(){
        accessorSupplierSpeedSlider.addChangeListener(e -> speedOfAccessorsCreation.set(accessorSupplierSpeedSlider.getValue()));
        engineSupplierSpeedSlider.addChangeListener(e -> speedOfEnginesCreation.set(engineSupplierSpeedSlider.getValue()));
        bodySupplierSpeedSlider.addChangeListener(e -> speedOfBodiesCreation.set(bodySupplierSpeedSlider.getValue()));
        dillersSpeedSlider.addChangeListener(e -> speedOfDillerWork.set(dillersSpeedSlider.getValue()));
    }
}
