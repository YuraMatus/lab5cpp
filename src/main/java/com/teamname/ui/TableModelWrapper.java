package com.teamname.ui;

import javax.swing.table.DefaultTableModel;

public class TableModelWrapper {
    private DefaultTableModel model;
    private final Object lock = new Object();
    private boolean withTotal;

    public TableModelWrapper(DefaultTableModel tableModel, boolean total){
        this.model = tableModel;
        this.withTotal = total;
    }

    public void increment(int row, int column){
        synchronized (lock){
            int value = (int) model.getValueAt(row,column);
            value++;

            model.setValueAt(value, row,column);
            if(withTotal) {
                recalculateTotal();
            }
            model.fireTableDataChanged();
        }
    }

    public void decrement(int row, int column){
        synchronized (lock){
            int value = (int) model.getValueAt(row,column);
            value--;

            model.setValueAt(value, row,column);
            if(withTotal) {
                recalculateTotal();
            }
            model.fireTableDataChanged();
        }
    }

    private void recalculateTotal(){
        int bodiesCount = (int) model.getValueAt(0,0);
        int enginesCount = (int) model.getValueAt(0,1);
        int accessorsCount = (int) model.getValueAt(0,2);

        int sum = bodiesCount + enginesCount + accessorsCount;

        model.setValueAt(sum, 1, 1);
    }
}
